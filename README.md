# dotfiles

repository to keep my dotfiles


## Dependencies (non-exhaustive list)

### Fonts

#### [Font-Awesome](https://fontawesome.com/)
Used extensively within i3 and polybar - it's a must have

#### [Fira-Code](https://github.com/tonsky/FiraCode)
Used in alacritty and polybar

### Programs for i3
this only applies if you want to use the i3 config

#### "Must-have" 
* [i3-gaps](https://github.com/Airblader/i3)
* [polybar](https://github.com/polybar/polybar) (replaces i3bar)
* [rofi](https://github.com/davatorium/rofi) (replaces dmenu)
* [nitrogen](https://github.com/l3ib/nitrogen) (background images)
* [i3cm](https://gitlab.com/Simerax/i3cm) this is used to generate the i3 config

#### Nice to have
* [i3lock](https://i3wm.org/i3lock/)
* [compton](https://github.com/chjj/compton)
* [redshift](https://github.com/jonls/redshift)
* [mpd](https://www.musicpd.org/)
* [alacritty](https://github.com/alacritty/alacritty)

