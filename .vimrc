call plug#begin()

Plug 'ctrlpvim/ctrlp.vim'
Plug 'elixir-editors/vim-elixir'
Plug 'vim-crystal/vim-crystal'
Plug 'slashmili/alchemist.vim'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'fatih/vim-go'

Plug 'jackguo380/vim-lsp-cxx-highlight'
Plug 'sbdchd/neoformat'
Plug 'ayu-theme/ayu-vim'
Plug 'sonph/onehalf', {'rtp': 'vim/'}
Plug 'ocaml/vim-ocaml'
call plug#end()

let mapleader = " "

filetype plugin indent on

set tabstop=4
set shiftwidth=4
set expandtab
set lazyredraw
set termguicolors
set cursorline
set ignorecase 
set smartcase
set number relativenumber
syntax on

nnoremap <leader>o :tabnew<CR>
nnoremap <leader>q :tabclose<CR>
nnoremap <leader>g :tabnew<CR>:G<CR>
nnoremap <leader>s :sp<CR>
nnoremap <leader>v :vsp<CR>



if system('suntell --isDay') == "true"
    " light, mirage or dark
    let ayucolor="light" 
    colo ayu
    let g:airline_theme='ayu'
else
    colo onehalfdark
    let g:airline_theme='onehalfdark'
endif
let g:airline_powerline_fonts = 1


map <C-h> <C-W>h
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-l> <C-W>l

nnoremap Q <nop>

"
" coc.nvim Settings
"
nmap gd <Plug>(coc-definition)


" Exit terminal mode with escape
tnoremap <Esc> <C-\><C-n>


" NeoVim does not reset the cursor style on exit
au VimLeave * set guicursor=a:hor100


" 
" Neoformat
"
let g:neoformat_cpp_clangformat = {'exe': 'clang-format','args': ['--style=LLVM'] }
let g:neoformat_enabled_cpp = ['clangformat']
let g:neoformat_enabled_c = ['clangformat']
nnoremap <F3> :Neoformat<CR>


"
" Crystal
" 
let g:crystal_auto_format=1
autocmd Filetype crystal setlocal ts=2 sw=2 expandtab

"
" Perl
"
autocmd BufRead *.t set filetype=perl

"
" "Enable" Json Comments
"
autocmd FileType json syntax match Comment +\/\/.\+$+
