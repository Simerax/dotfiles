#!/usr/bin/env bash

killall -q polybar
if [ $# -eq 0 ]
then
    polybar default
else
    polybar $@
fi
