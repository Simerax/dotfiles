#!/bin/sh

# Script to somewhat mimic the module/i3 polybar behaviour
# Features:
# * Only shows tags/workspaces with content
# * You can click on a tag/workspace to jump to it
#
# My tags are all just numbers so maybe this breaks on tags with words but it should probably not

print_tags() {
    # https://github.com/polybar/polybar/wiki/Formatting#action-a
	for tag in $(herbstclient tag_status); do
		name=${tag#?}
		state=${tag%$name}
		case "$state" in
		'#')
			printf '%%{R} %s %%{R}' "$name"
			;;
		# '+')
		# 	printf '%%{A1:herbstclient use %s:}%%{F#cccccc} %s %%{F-}%%{A}' "$name" "$name"
		# 	;;
		'!')
			printf '{A1:herbstclient use %s:}%%{F#f00} %s! %%{F-}%%{A}' "$name" "$name"
			;;
		'.')
			# printf '%%{A4:herbstclient use %s:}%%{A5:herbstclient use %s:}%%{A1:herbstclient use %s:}%%{F#cccccc} %s %%{F-}%%{A}%%{A}%%{A}' "$previous_tag" "$next_tag" "$name" "$name"
			;;
        ':')
			# printf '%%{A4:herbstclient use %s:}%%{A5:herbstclient use %s:}%%{A1:herbstclient use %s:}%%{B#101010} %s %%{B-}%%{A}%%{A}%%{A}' "$previous_tag" "$next_tag" "$name" "$name"
			printf '%%{A1:herbstclient use %s:}%%{F#606060} %s %%{F-}%%{A}' "$name" "$name"
            ;;
        '-')
            for mon in $(herbstclient list_monitors | cut -b1); do
                for tagmon in $(herbstclient tag_status $mon); do
                    namemon=${tagmon#?}
                    state=${tagmon%$namemon}
                    if [ "$name" == "$namemon" ]; then
                        case "$state" in
                            '+')
                                monname="$mon"
                                if [ "$mon" == "0" ]; then
                                    monname="l"
                                fi
                                if [ "$mon" == "1" ]; then
                                    monname="c"
                                fi
                                if [ "$mon" == "2" ]; then
                                    monname="r"
                                fi

			                    printf '%%{A1:herbstclient use %s:}%%{F#cccccc} %s%s %%{F-}%%{A}' "$name" "$name" "$monname"
                                # echo "$mon -> $name"
                                ;;
                        esac
                    fi
                done
            done
            ;;
		*)
			printf '%%{A1:herbstclient use %s:}%%{F#cccccc} %s %%{F-}%%{A}' "$name" "$name"
		esac
	done
	printf '\n'
}


print_tags

IFS="$(printf '\t')" herbstclient --idle | while read -r hook args; do
	case "$hook" in
	tag*)
		print_tags
		;;
	esac
done

