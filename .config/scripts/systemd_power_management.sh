#!/usr/bin/env bash

choice=$(echo -e "Shutdown\nReboot\nLogout" | rofi -dmenu -i -p "Do")

case "$choice" in
    'Shutdown')
        systemctl poweroff
        ;;
    'Reboot')
        systemctl reboot
        ;;
    'Logout')
        loginctl terminate-user $USER
        ;;
    '')
        ;;
    *)
        rofi -e "I don't know what you mean by '$choice'"
esac


