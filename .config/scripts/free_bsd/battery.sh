#!/usr/bin/env bash

battery=$(apm -l)
state=$(apm -b)

# add a ^ when charging
if (( $state == 3 )); then
    state='^'
else
    state=''
fi

printf '%s%s%%' "$state" "$battery"
