#!/usr/bin/env perl
use strict;

my $mode = $ARGV[0];

if (!defined $mode) {
    print STDERR "no mode defined do $0 add_fx or $0 remove_fx\n";
    exit(1);
}
unless($mode eq 'add_fx' || $mode eq 'remove_fx') {
    print STDERR "provided mode '$mode' not supported\n";
    exit(1);
}


# check if sink_main and sink_fx are ready
unless (`pactl list sinks short | grep sink_fx` && `pactl list sinks short | grep sink_main`) {
    print STDERR "sink_fx and/or sink_main not created\n";
    exit(1);
}


my $data = `pacmd list-sink-inputs | grep -e 'index:' -e 'media.name' -e 'application.name'`;

my %sinks;
my $current_sink = "";

foreach(split("\n", $data)) {
    if($_ =~ /index:\s(\d+)/) {
        $current_sink = $1;
    }
    if($current_sink && $_ =~ /media\.name = "(.*)"/) {
        $sinks{$current_sink}{media} = $1;
    }
    if($current_sink && $_ =~ /application\.name = "(.*)"/) {
        $sinks{$current_sink}{app} = $1;
    }
}

my $inputs = "";
foreach my $id (keys %sinks) {
    my $media = $sinks{$id}{media} // "";
    my $app = $sinks{$id}{app} // "";
    if($app) {
        $inputs .= "$id - $app | $media\n";
    }
}


my $selected_stream = `echo "$inputs" | rofi -dmenu -i -p "select stream" | awk '{print $1}'`;

if($selected_stream) {
    if($selected_stream =~ /^(\d+)/) {
        my $id = $1;

        if($mode eq 'add_fx') {
            `pactl move-sink-input $id sink_fx`
        } else {
            `pactl move-sink-input $id sink_main`
        }
    }
}
