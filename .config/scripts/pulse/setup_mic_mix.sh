#!/bin/bash


# check if sinks are already setup
if [ -n "$(pactl list sinks short | grep sink_fx)" ]
then
    rofi -e "It looks like Mic Mix is already active."
    exit 0
fi



microphone=$(pactl list short sources | grep -v "monitor" | awk '{print $2}' | rofi -dmenu -i -p "select microphone")

if [ -n "$microphone" ] 
then
    speakers=$(pactl list short sinks | awk '{print $2}' | rofi -dmenu -i -p "select speakers")
    if [ -n "$speakers" ]
    then
        # Straight up copied from https://wiki.archlinux.org/index.php/PulseAudio/Examples#Mixing_additional_audio_into_the_microphone's_audio
        echo "Setting up echo cancellation"
        pactl load-module module-echo-cancel use_master_format=1 aec_method=webrtc \
              aec_args="analog_gain_control=0\\ digital_gain_control=1\\ experimental_agc=1\\ noise_suppression=1\\ voice_detection=1\\ extended_filter=1" \
              source_master="$microphone" source_name=src_ec  source_properties=device.description=src_ec \
              sink_master="$speakers"     sink_name=sink_main sink_properties=device.description=sink_main
        
        echo "Creating virtual output devices"
        pactl load-module module-null-sink sink_name=sink_fx  sink_properties=device.description=sink_fx
        pactl load-module module-null-sink sink_name=sink_mix sink_properties=device.description=sink_mix
        
        echo "Creating loopbacks"
        pactl load-module module-loopback latency_msec=40 adjust_time=4 source=src_ec          sink=sink_mix
        pactl load-module module-loopback latency_msec=40 adjust_time=4 source=sink_fx.monitor sink=sink_mix
        pactl load-module module-loopback latency_msec=40 adjust_time=4 source=sink_fx.monitor sink=sink_main
        
        echo "Creating remaps"
        pactl load-module module-remap-source master=sink_mix.monitor \
              source_name=src_main source_properties="device.description=src_main"
        
        echo "Setting default devices"
        pactl set-default-source src_main
        pactl set-default-sink   sink_main

    fi
fi
