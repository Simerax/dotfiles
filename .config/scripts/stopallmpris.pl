#!/usr/bin/env perl
use warnings;
use strict;

my @players = split("\n", `playerctl --list-all`);
foreach my $p (@players) {
    `playerctl --player=$p stop`
}
