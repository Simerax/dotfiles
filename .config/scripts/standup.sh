#!/usr/bin/env bash

mode=$2

standupTime="30m"
sitdownTime="60m"

case "$mode" in
    'standup')
        notify-send "Aufstehen!"
        ogg123 /usr/share/sounds/freedesktop/stereo/complete.oga
        tomctl create $standupTime "$HOME/.config/scripts/standup.sh" "sitdown"
        ;;
    'sitdown')
        notify-send "Zeit zum hinsetzen"
        ogg123 /usr/share/sounds/freedesktop/stereo/complete.oga
        tomctl create $sitdownTime "$HOME/.config/scripts/standup.sh" "standup"
        ;;
esac


