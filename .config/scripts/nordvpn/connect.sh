#!/bin/sh


countries=$(nordvpn countries | perl -pe 's/\s+/\n/g' | grep -v -)
if [ -n "$countries" ]
then
    country=$(echo "$countries" | perl -pe 's/,//g' | rofi -dmenu -i)
    if [ -n "$country" ]
    then
        nordvpn connect $country
    fi
fi
