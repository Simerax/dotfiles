#!/bin/sh

n=$(whereis nordvpn)

if [ "$n" = "nordvpn:" ]; then
    echo "%{F#f00}%{A1:nordvpn c:}Nordvpn not installed%{A}%{F-}"
else
    STATUS=$(nordvpn status | grep Status | tr -d ' ' | cut -d ':' -f2)
    
    if [ "$STATUS" = "Connected" ]; then
        echo "%{F#82E0AA}%{A1:nordvpn d:}$(nordvpn status | grep City | cut -d ':' -f2)%{A}%{F-}"
    else
        echo "%{F#f00}%{A1:nordvpn c:}no vpn%{A}%{F-}"
    fi
fi

