#!/bin/sh

# pw-volume $@
# pw-volume status | jq .tooltip | xargs -I {} notify-send -t 500 Volume {}

pamixer $@
pamixer --get-volume-human | xargs -I {} notify-send -t 500 Volume {}
