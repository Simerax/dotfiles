#!/usr/bin/env bash

set term=alacritty

function s() {
    sleep 0.2
}

function swm() {
    swaymsg -t command $@
}

swm "exec alacritty"
swm "exec alacritty"
s
swm "move left"
s
swm "move left"
s
swm "focus right"
s
swm "resize grow width 400 px"
s
swm "resize grow width 400 px"
