#!/usr/bin/env bash

groupId=$1
curl -X PUT -d '{ "toggle": true }' "http://$DECONZ_HOST/api/$DECONZ_API_KEY/groups/$groupId/action"
