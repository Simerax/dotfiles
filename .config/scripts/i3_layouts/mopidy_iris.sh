#!/bin/sh

# Launch Mopidy Iris in separate qutebrowser window and move to workspace 7
#
# caveats
# The "Mopidy Iris" Window is found by qutebrowser class and title - this means that in case 
# there is another qutebrowser window with the title "Now playing" the script will probably do something silly
#

t=0.2

function focus_top {
    for i in {1..20}
    do
        i3-msg 'focus parent' 1>/dev/null 2>/dev/null
    done
}

to_ws=7
qutebrowser_ws=9

i3-msg "workspace $to_ws"
sleep $t
focus_top
sleep $t
i3-msg 'kill'
sleep $t
i3-msg "workspace $qutebrowser_ws"

qutebrowser --target=window http://localhost:6680/iris/queue &

sleep 4

i3-msg "workspace $to_ws"
sleep $t
i3-msg "[class=\"qutebrowser\" title=\"Now playing\"] move to workspace $to_ws"
sleep $t
i3-msg 'exec i3-sensible-terminal'
sleep $t
i3-msg 'exec i3-sensible-terminal'
sleep $t
i3-msg 'move left ; move left'
sleep $t
i3-msg 'focus right; resize grow width 20 px or 20 ppt'
sleep $t


