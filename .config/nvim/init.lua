local cmd = vim.cmd
local fn = vim.fn
local g = vim.g
local opt = vim.opt

local function bootstrap_packer()
    local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({'git', 'clone', '--depth=1', 'https://github.com/wbthomason/packer.nvim', install_path})
    end
end

local function map(mode, lhs, rhs, opts)
    local options = {noremap = true}
    if opts then
        options = vim.tbl_extend('force', options, opts)
    end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

bootstrap_packer()


require('packer').startup(function()
    use { "wbthomason/packer.nvim"          }
    use { "sonph/onehalf", rtp = "vim/"     }
    use { "tpope/vim-fugitive"              }
    use { "vim-airline/vim-airline"         }
    use { "vim-airline/vim-airline-themes"  }
    use { "fatih/vim-go"                    }
    use { "Luxed/ayu-vim"                   }
    use { "nvim-treesitter/nvim-treesitter" }
    use { "neovim/nvim-lspconfig"           }
    use { "ziglang/zig.vim"                 }
    use { "gleam-lang/gleam.vim"            }
    use { "elixir-editors/vim-elixir"       }
    use { "vim-crystal/vim-crystal"         }
    use { "jvirtanen/vim-hcl"               }
    use { "lervag/vimtex"                   }
    use { "stevearc/dressing.nvim"          }
    use { "akinsho/flutter-tools.nvim",     requires = { "nvim-lua/plenary.nvim" } }
    use { "lewis6991/gitsigns.nvim",        requires = { "nvim-lua/plenary.nvim" } }
    use { "nvim-telescope/telescope.nvim",  requires = { "nvim-lua/plenary.nvim" } }
    use { "ray-x/lsp_signature.nvim" }

    use { "hrsh7th/nvim-cmp" }
    use { "hrsh7th/cmp-nvim-lsp" }
    use { "hrsh7th/cmp-vsnip" }
    use { "hrsh7th/vim-vsnip" }
end)

local cmp = require('cmp')

cmp.setup({
    snippet = {
        expand = function(args) vim.fn["vsnip#anonymous"](args.body) end,
    },
    -- window = {
    --     completion = cmp.config.window.bordered(),
    -- }
    mapping = cmp.mapping.preset.insert({
        ['<CR>'] = cmp.mapping.confirm({select = true}),
        ['<C-e>'] = cmp.mapping.abort(),
    }),
    sources = cmp.config.sources({
        {name = 'nvim_lsp' },
        {name = 'vsnip' },
    }, {
        {name = 'buffer'}
    }),
})

require('lsp_signature').setup({
    bind = true,
    handler_opts = {
        border = "rounded"
    }
})

vim.g.markdown_fenced_languages = {
    "ts=typescript"
}

local lspconfig = require('lspconfig')

lspconfig.gleam.setup{}
lspconfig.denols.setup{}

lspconfig.elixirls.setup{
    cmd = { "elixirls" },
}

lspconfig.zls.setup{}

lspconfig.raku_navigator.setup{
    cmd = {'node', '/home/fx/code/github.com/bscan/RakuNavigator/server/out/server.js', '--stdio'},
}


require('gitsigns').setup()

-- g.dashboard_default_executive='telescope'


g.mapleader = " "
opt.tabstop = 4
opt.shiftwidth = 4
opt.expandtab = true
opt.lazyredraw = true
opt.termguicolors = true
opt.cursorline = true
opt.ignorecase = true
opt.smartcase = true
opt.number = true
opt.relativenumber = true
opt.syntax = "on"


map('n', '<leader>f', '<cmd>lua vim.lsp.buf.formatting()<CR>')
map('n', 'gd',        '<cmd>lua vim.lsp.buf.definition()<CR>')
map('n', '<leader>h', '<cmd>lua vim.lsp.buf.hover()<CR>')
map('n', '<C-g>',     ':Telescope live_grep<CR>')
map('n', '<C-p>',     ':Telescope find_files<CR>')
map('',  '<leader>o', ':tabnew<CR>')
map('',  '<leader>q', ':tabclose<CR>')
map('',  '<leader>g', ':tabnew<CR>:G<CR>')
map('',  '<leader>s', ':sp<CR>')
map('',  '<leader>v', ':vsp<CR>')
map('',  '<C-h>',     '<C-W>h')
map('',  '<C-j>',     '<C-W>j')
map('',  '<C-k>',     '<C-W>k')
map('',  '<C-l>',     '<C-W>l')
map('',  'Q',         '<nop>')

cmd('autocmd VimLeave * set guicursor=a:hor100')
cmd('autocmd Filetype crystal setlocal ts=2 sw=2 expandtab')
cmd('autocmd BufRead *.t set filetype=perl')
cmd('set nofoldenable')


if fn.system({'suntell', '--isDay'}) == "true" then
    g.ayucolor='light'
    g.airline_theme = 'ayu_light'
    cmd 'colorscheme ayu'
else
    g.ayucolor='mirage'
    g.airline_theme = 'ayu_mirage'
    cmd 'colorscheme ayu'
    -- g.airline_theme='onehalfdark'
    -- cmd 'colorscheme onehalfdark'
end

g.airline_powerline_fonts = 1
g.vimtex_view_general_viewer = 'evince'
