#!/bin/sh
killall -q picom

# In combination with a super ultra wide monitor (5120x1440) picom
# messes with your screen layout/resolution
# we make sure to wait for i3 to fully start before starting picom
sleep 5

picom &
