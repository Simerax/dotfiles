{
    // "layer": "top", // Waybar at top layer
    // "position": "bottom", // Waybar position (top|bottom|left|right)
    "position": "bottom",

    // "width": 1280, // Waybar width
    // Choose the order of the modules

    "modules-left":[
        "tray",
        "sway/workspaces",
        "sway/mode",
        "custom/media"
    ],

    "modules-center": [
        "clock"
    ],

    "modules-right":[
        "disk#root",
        "network",
        "battery",
        "custom/pia_vpn"
    ],
        //"mpd",
        //"custom/headphone",
        //"disk#home",
        //"idle_inhibitor"

    "custom/headphone": {
        "exec": "headsetcontrol -c -b",
        "format": " HP: {}% ",
        "interval": 120
    },

    "disk#root": {
        "tooltip-format": "{path} : {used} von {total} verbraucht ({percentage_used}%)",
        "interval": 60,
        "format": " {path} : {free} ",
        "path":"/"
    },
    "disk#home": {
        "tooltip-format": "{path} : {used} von {total} verbraucht ({percentage_used}%)",
        "interval": 60,
        "format": " {path} : {free} ",
        "path":"/home"
    },


    "tray": {
        // "icon-size": 21,
        "spacing": 10
    },

    "clock": {
        "tooltip-format": "<big>{:%B %Y}</big>\n<tt><small>{calendar}</small></tt>",
        "format": "{:%H:%M %A %d.%m.%Y - KW %U}",
        "calendar": {
            "mode"          : "year",
            "mode-mon-col"  : 3,
            "weeks-pos"     : "right",
            "on-scroll"     : 1,
            "on-click-right": "mode",
            "format": {
                "months":     "<span color='#ffead3'><b>{}</b></span>",
                "days":       "<span color='#ecc6d9'><b>{}</b></span>",
                "weeks":      "<span color='#99ffdd'><b>KW{}</b></span>",
                "weekdays":   "<span color='#ffcc66'><b>{}</b></span>",
                "today":      "<span color='#ff6699'><b><u>{}</u></b></span>"
            }
        }
    },


    "sway/mode": {
        "format": "<span style=\"italic\">{}</span>"
    },


    "network": {
        // "interface": "wlp2*", // (Optional) To force the use of this interface
        "format-wifi": "{essid} ({signalStrength}%) ",
        "format-ethernet": "{ifname}: {ipaddr}/{cidr} ",
        "format-linked": "{ifname} (No IP) ",
        "format-disconnected": "Disconnected ⚠",
        "format-alt": "{ifname}: {ipaddr}/{cidr}",
        "tooltip-format": "{essid} | {ifname} | {ipaddr}"
    },


    "sway/workspaces": {
        "enable-bar-scroll": true,
        "disable-scroll-wraparound": true,
        "format": "{name}{icon}",
        "format-icons": {
            "2": " ",
            "7": " ",
            "9": " ",
            "10": " " ,
            "default": ""
        }
    },


    "mpd": {
        "format": "{artist} - {title}",
        "format-stopped": "",
        "format-disconnected": "MPD Disconnected",
        "consume-icons": {
            "on": " "
        },
        "random-icons": {
            "off": "<span color=\"#f53c3c\"></span> ",
            "on": " "
        },
        "repeat-icons": {
            "on": " "
        },
        "single-icons": {
            "on": "1 "
        },
        "state-icons": {
            "paused": "",
            "playing": ""
        },
        //"tooltip-format": "MPD (connected)", 
        "tooltip-format": "{artist} - {title} (Album: {album}) ({elapsedTime:%M:%S}/{totalTime:%M:%S})",
        "tooltip-format-disconnected": "MPD (disconnected)"
    },


    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },

    "battery": {
        "interval": 60,
        "states": {
            "warning": 25,
            "critical": 10
        },
        "format-discharging": "{capacity}%",
        "format-charging": "{capacity}%"
    },
    "custom/pia_vpn": {
        "exec-if": "pgrep pia-daemon",
        "on-click": "$HOME/.config/waybar/vpn/pia.sh toggle",
        "on-click-right": "$HOME/.config/waybar/vpn/pia.sh connectregion",
        "exec": "$HOME/.config/waybar/vpn/pia.sh status",
        "return-type": "json",
        "interval": 5
    }
}

// vim:filetype=json
