#!/usr/bin/env bash

# Check if piactl is available
if [ "$(whereis piactl)" = "piactl:" ]; then
    json_echo "PIA not installed (missing piactl command)"
fi

function json_echo() {
    echo '{"text": "'$1'", "tooltip": "'$2'", "class": ""}'
    exit
}

function get_status() {
    status=$(piactl get connectionstate)
    # Disconnected, Connecting, Connected, Interrupted, Reconnecting, DisconnectingToReconnect, Disconnecting
    case $status in
        Disconnected)
            json_echo "No VPN " "VPN is currently disconnected"
            ;;
        Connecting)
            json_echo "VPN: Connecting..." 
            ;;
        Connected)
            json_echo "VPN: $(piactl get region)" "$(piactl get protocol) $(piactl get vpnip)"
            ;;
        Interrupted)
            json_echo "VPN: Interrupted" "The VPN Connection is currently interrupted"
            ;;
        Reconnecting)
            json_echo "VPN: Reconnecting..." "The VPN is currently trying to reconnect"
            ;;
        DisconnectingToReconnect)
            json_echo "VPN: About to Reconnect..." "Currently Disconnecting to then reconnect"
            ;;
        Disconnecting)
            json_echo "VPN: Disconnecting..."
            ;;
        *)
            json_echo "No VPN " "The current status '$status' is unknown..."
    esac
}

function toggle_conn() {
    status=$(piactl get connectionstate)
    case $status in
        Disconnected)
            piactl connect
            ;;
        Connected)
            piactl disconnect
            ;;
        *)
            # do nothing
    esac
}

function connect_region() {
    selected_region=$(piactl get regions | rofi -dmenu -i -p "Select Region")
    if [ -n "$selected_region" ];
    then
        piactl set region $selected_region
        piactl connect
    fi
}




cmd=$1

if [ "$cmd" = "" ]; then
    cmd="status"
fi

case $cmd in
    status)
        get_status
        ;;
    toggle)
        toggle_conn
        ;;
    connectregion)
        connect_region
        ;;
    *)
        invalid_cmd
        ;;
esac
